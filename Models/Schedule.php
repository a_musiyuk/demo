<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 11 Jan 2018 14:42:31 +0000.
 */

namespace App\Models;
use App\Models\Catalogs\SchoolClass;
use App\Models\Catalogs\Student;
use App\Models\Catalogs\Subject;
use App\Models\Catalogs\Teacher;
use Carbon\Carbon;


/**
 * Class Schedule
 *
 * @property int $id
 * @property int $day
 * @property int $class_id
 * @property int $group_id
 * @property int $timeslot_id
 * @property int $room_id
 * @property int $subject_id
 * @property int $teacher_id
 * @property int $school_id
 * @package App\Models
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Schedule whereClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Schedule whereDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Schedule whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Schedule whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Schedule whereRoomId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Schedule whereSchoolId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Schedule whereSubjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Schedule whereTeacherId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Schedule whereTimeslotId($value)
 * @mixin \Eloquent
 */
class Schedule extends BaseModel
{
	protected $table = 'schedule';

	protected $casts = [
		'day' => 'int',
		'class_id' => 'int',
		'group_id' => 'int',
		'timeslot_id' => 'int',
		'room_id' => 'int',
		'subject_id' => 'int',
		'teacher_id' => 'int',
		'school_id' => 'int',
        'have_groups'=>'int'
	];

	protected $fillable = [
		'day',
		'class_id',
		'group_id',
		'timeslot_id',
		'room_id',
		'subject_id',
		'teacher_id',
		'school_id',
        'schedule_period_id',
        'even_odd',
        'have_groups'
	];


    public function schoolclass()
    {
        return $this->belongsTo('App\Models\Catalogs\SchoolClass','class_id');
    }

    public function subject()
    {
        return $this->belongsTo('App\Models\Catalogs\Subject');
    }

    public function teacher()
    {
        return $this->belongsTo('App\Models\Catalogs\Teacher');
    }

    public function room()
    {
        return $this->belongsTo('App\Models\Catalogs\Room');
    }


    public function groups(){
        return $this->hasMany(ScheduleCustomGroup::class);
    }

    public static function getDayScheduleStudent($student,$date) {

        $num_day = $date->dayOfWeek;
        $even_week = ($date->weekOfYear%2==0);
        return Schedule::with('room','subject','teacher')->
        where('class_id',$student->class_id)->
        where('day',$num_day-1)->
        where('schedule_period_id',1)->
        where('even_odd',$even_week)->get();
    }

    public static function getDayScheduleTeacher($teacher_id,$date) {

        $date_crb = Carbon::createFromFormat($date);

        $even_week = ($date_crb->weekOfYear%2==0);
        $num_day = $date_crb->dayOfWeek;
        $teacher  = Teacher::find($teacher_id);
        return Schedule::with('room','subject','schoolclass')->
        where('school_id',$teacher->school_id)->
        where('day',$num_day)->
        where('teacher_id',$teacher_id)->
        where('even_odd',$even_week)->get();
    }

}
