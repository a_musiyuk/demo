<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Resident extends Model
{
    protected $primaryKey='id';

    protected $dates=['document_own_date','date_own'];

    protected $fillable=[
        'house_id',
        'company_id',
        'peoples_count',
        'number_appart',
        'totalarea_apart',
        'heat_apart',
        'lname_resident',
        'name_resident',
        'mname_resident',
        'fullname_resident',
        'telefon_home',
        'telefon_contact',
        'email',
        'percent_own',
        'acc',
        'document_own_number',
        'document_own',
        'external_key',

        'date_own',
        'document_own_date',
        'document_own_comment',
        'comment',
        'external_key',
        'lifesquare_appart',
        'addedsquare_appart',
        'type_appart',
        'flat_count'


    ];

    protected $attributes = [
        'peoples_count'=>0,
        'number_appart'=>"",
        'totalarea_apart'=>0,
        'heat_apart'=>0,
        'lname_resident'=>'',
        'name_resident'=>'',
        'mname_resident'=>'',
        'email'=>'',
        'fullname_resident'=>'',
        'telefon_home'=>'',
        'telefon_contact'=>'',
        'percent_own'=>00,
        'acc'=>'',
        'flat_count'=>0,
        'document_own_number'=>'',
        'addedsquare_appart'=>0
    ];

    protected static function  boot(){
        parent::boot();
        static::addGlobalScope(new \App\Models\Scopes\OnlyCurrentCompany());
    }

    public static function getBorgAndPaymentResidents()
    {
        $sqlRAW="resident_id,SUM(IF(date_transaction<'".Auth::user()->getCurPeriod()->startOfMonth()->format('Y-m-d')."',IF(incoming=1 or incoming=4,sum,-sum),0)) as borg,SUM(IF(incoming=2 AND date_transaction>='".Auth::user()->getCurPeriod()->startOfMonth()->format('Y-m-d')."',sum,0)) as payment";

        return DB::table('transactions')->select(DB::raw($sqlRAW))
            ->where('transactions.company_id','=',Auth::user()->company_id)
            ->whereRaw("date_transaction <= '".Auth::user()->getCurPeriod()->endOfMonth()->format('Y-m-d')."'")
            ->groupBy('resident_id')
            ->get();

    }

    public function house()
    {
        return $this->belongsTo('App\Models\House');
    }


    public function apartscounter(){
        return $this->hasMany('App\Models\ApartsCounter');
    }

    public function transaction(){
        return $this->hasMany('App\Models\Transaction');
    }

    public function subsidy(){
        return $this->hasMany('App\Models\Subsidy');
    }

    public function residentpriviliges(){
        return $this->hasMany('App\Models\ResidentPrivilige');
    }

    public function apartmentpersons()
    {
        return $this->hasMany('App\Models\ApartmentPerson');
    }

    public function value_attributes(){
        return $this->hasMany('App\Models\ValuesAttributesResident');
    }

    public function transactionForInvoice()
    {
        return $this->hasMany('App\Models\Transaction')->where('incoming','=',1)
            ->whereMonth('date_transaction',Auth::user()->getCurPeriod()->month)
            ->whereYear('date_transaction', Auth::user()->getCurPeriod()->year);
    }

    public static  function getResidents(){
        return Resident::where('company_id','=',Auth::user()->company_id)->get();
    }


    public function IndividualRate() {
        return $this->hasMany('App\Models\Individualapplyrate'); //;
    }


    public function haveIndividualRate($rate_id=0) {
        return count($this->hasMany('App\Models\Individualapplyrate')->where('rate_id','=',$rate_id)->where('isActive','=',true)->get())>0;
    }


    public function getAdress()
    {
        return $this->house->getAdress().', кв. '.$this->number_appart;
    }

    public function present(){
        return 'Л/с '.$this->acc." ".$this->fullname_resident.' - кв. '.$this->number_appart;
    }


    public function getTransactions(){

        $sqlRAW=' SUM(IF(incoming=1,sum,0)) as income,SUM(IF(incoming=5,sum,0)) as privilige, SUM(IF(incoming=2,sum,0)) as payment,'
                .' SUM(IF(incoming=3,sum,0)) as subsidy, SUM(IF(incoming=4,sum,0)) as correct,'
                .'comment, shortname_grouprate as shortname_grouprate ';

        return DB::table('transactions')->select(DB::raw($sqlRAW),'date_transaction')
            ->leftJoin('grouprates', 'transactions.grouprate_id', '=', 'grouprates.id')
            ->groupBy('date_transaction','shortname_grouprate','comment')
            ->orderBy('date_transaction','asc')
            ->where('transactions.company_id','=',$this->company_id)
            ->where('transactions.resident_id','=',$this->id)
            ->get();
    }

    public function getAttributeLastData($id_attrib,$date_data){
        return $this->value_attributes()->whereDate('period','<=',$date_data)->where('attributes_residents_id',$id_attrib)->orderBy('period','desc')->first();
    }

    public function formatAttributeValue($attrib,$resault)
    {
        if ($attrib->typestoreddata=='string'){
            $value = $resault->value_string;
        }
        elseif($attrib->typestoreddata=='boolean'){
            $value = $resault->value_boolean;
        }
        elseif($attrib->typestoreddata=='date'){
            $value =  $resault->value_date;
        }
        elseif($attrib->typestoreddata=='decimal'){
            $value=$resault->value_decimal;
        }
        elseif($attrib->typestoreddata=='int'){
            $value = $resault->value_int;
        }
        return $value;
    }
    public function getAttributeLastDataByKey($attrib,$date_data){

        if ($attrib->key_attribute=='document_own') {
            $attrib=AttributesResident::where('key_attribute','document_own')->first();
            $document_own =  $this->value_attributes()->whereDate('period','<=',$date_data)->where('attributes_residents_id',$attrib->id)->orderBy('period','desc')->first();
            $attrib=AttributesResident::where('key_attribute','document_own_number')->first();
            $document_own_number =  $this->value_attributes()->whereDate('period','<=',$date_data)->where('attributes_residents_id',$attrib->id)->orderBy('period','desc')->first();
            $attrib=AttributesResident::where('key_attribute','document_own_date')->first();
            $document_own_date =  $this->value_attributes()->whereDate('period','<=',$date_data)->where('attributes_residents_id',$attrib->id)->orderBy('period','desc')->first();
            $attrib=AttributesResident::where('key_attribute','date_own')->first();
            $date_own =  $this->value_attributes()->whereDate('period','<=',$date_data)->where('attributes_residents_id',$attrib->id)->orderBy('period','desc')->first();
            $attrib=AttributesResident::where('key_attribute','percent_own')->first();
            $percent_own =  $this->value_attributes()->whereDate('period','<=',$date_data)->where('attributes_residents_id',$attrib->id)->orderBy('period','desc')->first();

            return $document_own->value_string.'- №'.$document_own_number->value_string.'- от '.$document_own_date->value_date->format('d-m-Y').'-'.$date_own->value_date->format('d-m-Y').'-'.$percent_own->value_decimal ;
        }
        elseif ($attrib->key_attribute=='lname_resident') {
            $attrib=AttributesResident::where('key_attribute','lname_resident')->first();
            $lname_resident =  $this->value_attributes()->whereDate('period','<=',$date_data)->where('attributes_residents_id',$attrib->id)->orderBy('period','desc')->first();
            $attrib=AttributesResident::where('key_attribute','name_resident')->first();
            $name_resident =  $this->value_attributes()->whereDate('period','<=',$date_data)->where('attributes_residents_id',$attrib->id)->orderBy('period','desc')->first();
            $attrib=AttributesResident::where('key_attribute','mname_resident')->first();
            $mname_resident =  $this->value_attributes()->whereDate('period','<=',$date_data)->where('attributes_residents_id',$attrib->id)->orderBy('period','desc')->first();
            return $lname_resident->value_string.' '.$name_resident->value_string.' '.$mname_resident->value_string;
        } else {
            $resault =  $this->value_attributes()->whereDate('period','<=',$date_data)->where('attributes_residents_id',$attrib->id)->orderBy('period','desc')->first();
            return $this->formatAttributeValue($attrib,$resault);
        }


    }

    public  function getFieldsWithValue(){
        return AttributesResident::where('based',false)->get();

    }

    public function getHistoryFields()
    {
        $res = AttributesResident::where("based",1)->where('isinactive',0)->orderBy('order')->get();

        $arrExcept[] = 'lname_resident';
        $arrExcept[] = 'name_resident';
        $arrExcept[] = 'mname_resident';
        $arrExcept[] = 'percent_own';
        $arrExcept[] = 'document_own_number';
        $arrExcept[] = 'document_own_date';
        $arrExcept[] = 'date_own';

        $res = $res->reject(function ($value, $key) use ($arrExcept) {
            return array_search($value['key_attribute'],$arrExcept);
        });


        return $res->all();
    }

    public function getHistoryList()
    {
        $res =  DB::table('values_attributes_residents')->select('values_attributes_residents.period')->
        join('attributes_residents', 'attributes_residents.id', '=', 'values_attributes_residents.attributes_residents_id')->
        where('values_attributes_residents.resident_id', '=', $this->id)->
        where('values_attributes_residents.company_id', '=', $this->company_id)->
        groupBy('period')->orderBy('period')->get();
        return $res;
    }

    public function getValueAttributeOnDate($period,$key_attrib)
    {

    }

    public function getPeopleCount()
    {
        $persCount=0;
        if ($this->apartmentpersons) {
            $persCount =  $this->apartmentpersons->count();
        }
        if ($persCount==0) {
            $persCount = $this->peoples_count;
        }
        return $persCount;
    }

    public function getPrivName(){
        $res='немає пільг';
        if ($this->residentpriviliges) {
            foreach($this->residentpriviliges as $curpriv){
                $res = $curpriv->privilige->name_privilige;
                break;
            }
            return $res;
        } else {
            return  $res;
        }
    }
}


