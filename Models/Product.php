<?php

namespace App\Models;

class Product extends BaseModel
{
    protected $guarded=['id'];

    public function getActionButtonsAttribute(){
        return   '<a href="javascript:showDialog(1,'.$this->id.')" name="add_to_cart" class="btn btn-xs  btn-info"> <i class="fa fa-cart-plus" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.backend.products.actions.add_to_cart').'"></i></a>'.
                '<a href="javascript:showDialog(2,'.$this->id.')" name="product_info" class="btn btn-xs btn-info"><i class="fa fa-info" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.backend.products.actions.product_info').'"></i></a> '.
                '<a href="javascript:showDialog(3,'.$this->id.')" name="product_history" class="btn btn-xs btn-info"><i class="fa  fa-history" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.backend.products.actions.product_history').'"></i></a>';
    }


    public function getPrice(){
        return 150;
    }

    public function prices()
    {
        return $this->hasMany('App\Models\ProductPrice');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Contract');
    }
}
