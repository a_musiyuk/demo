<?php

namespace App\Http\Controllers\Backend;

use App\Models\Catalogs\CustomGroupDet;
use App\Models\Catalogs\SchoolClass;
use App\Models\Schedule;
use App\Models\ScheduleManual;
use App\Models\SchedulePeriod;
use App\Models\Timeslot;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $week_days = getDaysWeek(true);
        $schoolclasses = SchoolClass::all();
        $schedule= Schedule::all();
        $timeslots = Timeslot::all();
        return view('backend.schedule.schedule_index',['week_days'=>$week_days,'schoolclasses'=>$schoolclasses,'schedule'=>$schedule,'timeslots'=>$timeslots]);
    }


    public function getScheduleStudent(Request $request){

        $days = getDaysWeek(true);
        $days[]='Розклад дзвінків';
        $limit_lesson=7;
        $day_class=['list-group-item-primary','list-group-item-success','list-group-item-danger','list-group-item-warning','list-group-item-info','list-group-item-secondary'];

        return view('backend.student.schedule_index',compact('days','limit_lesson','day_class'));
    }

    public function getScheduleTeacher(Request $request){

        $days = getDaysWeek(true);
        $days[]='Розклад дзвінків';

        $limit_lesson=7;
        $day_class=['list-group-item-primary','list-group-item-success','list-group-item-danger','list-group-item-warning','list-group-item-info','list-group-item-secondary'];

        return view('backend.teacher.schedule_index',compact('days','limit_lesson','day_class'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return response()->json(['STATUS'=>'OK','ERR'=>'','message'=>$request->all()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function show(Schedule $schedule)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function edit(Schedule $schedule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Schedule $schedule)
    {
        return response()->json(['STATUS'=>'OK','ERR'=>'','message'=>$request->all()]);

    }


    public function save(Request $request)
    {
        if ($request->input('id')) { //udate
            $schedule = Schedule::findOrFail($request->input('id'));
        } else { //create
            $schedule = new Schedule();
        }

        $data = $request->all();

        if (strpos($data['custom_groups'],','))
            $groups = explode(',',$data['custom_groups']);
        else
            $groups[]=$data['custom_groups'];

        $data['have_groups']= ($data['type']=='subject' ? 0 : 1);
        unset($data['type']);
        unset($data['custom_groups']);
        unset($data['id']);

        foreach ($data as $key=>$value) {
            if ($value=='' OR $value=='null' OR $value=='undefined'){
                $data[$key]=0;
            }
        }
        $data['school_id']=$request->session()->get('school_id',$request->user()->school_id);

        $schedule->fill($data);
        $schedule->save();
        $schedule->groups()->delete();
        if ($data['have_groups']) {
            foreach ($groups as $group){
                $schedule->groups()->create(['custom_group_det_id'=>$group,'schedule_manual_id'=>0]);
            }

        }

        return response()->json(['STATUS'=>'OK','ERR'=>'','message'=>trans('alerts.backend.scheduleslots.updated')]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function destroy(Schedule $schedule)
    {
        //
    }



    /////Schedule periods
    public function getSchedulePeriod(Request $request)
    {
            if ($request->ajax()){
                if ($request->query('schedule_period_id')) {
                    $schedulePeriod = SchedulePeriod::findOrFail($request->query('schedule_period_id'));
                    $status="OK";
                    $result=['begin_date'=>$schedulePeriod->begin_date->format('d.m.Y'),
                             'end_date'=>$schedulePeriod->end_date->format('d.m.Y')];
                } else {
                    $status="ERR";
                    $result=[];
                }
                return response()->json(['STATUS'=>$status,'data'=>$result]);
            }
    }

    public function setSchedulePeriod(Request $request)
    {
        if ($request->ajax()){
            if ($request->input('schedule_period_id')) {
                $schedulePeriod = SchedulePeriod::findOrFail($request->input('schedule_period_id'));
            } else {
                $schedulePeriod = new SchedulePeriod();
                $schedulePeriod->school_id = session()->get('school_id',Auth::user()->school_id);
            }
            $schedulePeriod->begin_date = Carbon::createFromFormat('d.m.Y',$request->input('begin_date'));
            $schedulePeriod->end_date =  Carbon::createFromFormat('d.m.Y',$request->input('end_date'));
            if ($schedulePeriod->save()) {
                $messOk=trans('alerts.schedule.scheduleperiod.created');
            } else {
                $messOk=trans('alerts.schedule.scheduleperiod.updated');
            }
            return response()->json(['STATUS'=>'OK','ERR'=>'','message'=>$messOk]);
        }
    }

    public function checkSchedulePeriod(Request $request)
    {
        return response()->json(['STATUS'=>'OK','ERR'=>'','message'=>'OK']);
    }

    //////////////////////////////

    public function getSchedule(Request $request) {
        if ($request->ajax()) {


            $arrGrp=[];
            $curSchool = getcurSchool();
            $use_odd_even = $curSchool->use_even_odd;
            $builder = Schedule::with(['subject','room','teacher']);
            if ($request->has('period')&&$curSchool->use_period) {
                $builder->where('schedule_period_id',$request->query('period'));
            }

            if ($request->has('odd_even')&&$use_odd_even) {
                $builder->where('even_odd',$request->query('odd_even'));
            }
            $subjects = $builder->get()->toArray();
            $manual = ScheduleManual::with(['subject','room','teacher'])->get();
            foreach ($subjects as $key=>$slot){
                $m_schedule = $manual->where('day',$slot['day'])->where('class_id',$slot['class_id'])->where('timeslot_id',$slot['timeslot_id'])->first();
                $subjects[$key]['have_manual'] = !empty($m_schedule);

                if ($slot['have_groups']){
                    $groups =CustomGroupDet::with(['subject','room','teacher'])->whereRaw('id IN (SELECT custom_group_det_id FROM schedule_custom_groups WHERE schedule_id='.$slot['id'].')')->get();

                    $select='
                    <ul class="nav nav-tabs">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Довільні групи</a>
                            <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 39px, 0px);">';
                            foreach ($groups as $group)
                            $select.=  '<div class="dropdown-item">
                                    <div class="desc2">
                                        <div class="title span_cg_subject">'.($group->subject ? $group->subject->name : '').'</div>
                                        <small class="span_cg_teacher">'.($group->teacher ? $group->teacher->name_short : '').'(каб. '.($group->room ? $group->room->name : '').')</small>
                                    </div>
                                </div>';
                    $select.=
                    '        </div>
                        </li>
                    
                    </ul>';
                    $subjects[$key]['groups_html']=$select;

                }else{
                    //для обратной совместимости
                    
                    $subjects[$key]['groups_html']='';
                }
            }

            $arrRes=['status'=>'OK','data'=>$subjects];
            return response()->json($arrRes);
        }
    }

    public function fillSchedulePeriod(Request $request){


        if ($request->ajax()){

            $validator = Validator::make($request->all(), [
                'from_schedule_period_id' => 'required',
                'from_even_odd' => 'required',
                'to_schedule_period_id' => 'required',
                'to_even_odd' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status'=>'ERR','err'=>'NO REQUIRED FIELDS']);
            } else {

                Log::info('schedule_period_id '.$request->input('to_schedule_period_id'));
                Log::info('even_odd '.$request->input('to_even_odd'));


                $haveSlots = Schedule::where('schedule_period_id',$request->input('to_schedule_period_id'))->
                             where('even_odd',$request->input('to_even_odd'))->
                             where('subject_id','>',0)->
                             where('teacher_id','>',0)->
                             where('room_id','>',0)->
                             get()->first();
                if ($haveSlots) {
                    return response()->json(['status'=>'ERR','err'=>'SCHEDULE_ALLREADY_FILLED','message'=>'']);
                } else {
                    $fromSlots = Schedule::where('schedule_period_id',$request->input('from_schedule_period_id'))->
                                           where('even_odd',$request->input('from_even_odd'))->get()->toArray();
                    Schedule::where('schedule_period_id',$request->input('to_schedule_period_id'))->
                              where('even_odd',$request->input('to_even_odd'))->delete();

                    foreach ($fromSlots as $fromSlot){
                        $newSlot = new Schedule();
                        $newSlot->fill($fromSlot);
                        $newSlot->schedule_period_id =$request->input('to_schedule_period_id');
                        $newSlot->even_odd =$request->input('to_even_odd');
                        $newSlot->save();
                    };
                    $messOk=trans('alerts.backend.schedule.filled');
                    return response()->json(['status'=>'OK','err'=>'','message'=>$messOk]);
                }

            }
        }
    }
    ////////////////////////

    public function ScheduleCalls(Request $request)
    {
        return view('backend.scheduler_calls');
    }

    public function ScheduleCallsList(Request $request)
    {
        $timeslots=Timeslot::select('timeslots.*');

        $number = 0;
        return DataTables::of($timeslots)

            ->addColumn('type_name', function($timeslot) {
                return  $timeslot->type=='lesson' ? trans('labels.schedule.calls.lesson') : trans('labels.schedule.calls.break');
                //return  '<select style="width: 100%; height: 100%;" class="form-control" name="type[]"><option '.($timeslot->type=='lesson' ? ' selected ' : '').' value="lesson">урок</option><option '.($timeslot->type=='break' ? ' selected ' : '').'value="break">перерва</option></select>';
            })
            ->addColumn('number', function($timeslot) use (&$number) {
                $number++;
                return  $number;
            })

            ->editColumn('start', function($timeslot) {
                return  '<input  maxlength="5" style="width: 100%; height: 100%;" class="form-control from_till_input" name="start[]" value="'.$timeslot->start.'">';
            })
            ->editColumn('stop', function($timeslot) {
                return  '<input  maxlength="5" style="width: 100%; height: 100%;" class="form-control from_till_input" name="stop[]" value="'.$timeslot->stop.'">';
            })
            ->addColumn('action', function($timeslot) {
                return  $timeslot->action_buttons;
            })
            ->rawColumns(['type_name','start','stop','action'])
            
            ->filter(function ($query) use ($request) {
                if ($request->has('client_id')&&$request->get('client_id')) {
                    $query->where('client_id',"{$request->get('client_id')}");
                }

                if ($request->has('contract_id')&&$request->get('contract_id')) {
                    $query->where('contract_id', "{$request->get('contract_id')}");
                }
            })
            ->make(true);
    }

    public function ScheduleCallsDelete(Request $request)
    {
        if ($request->input('timeslot_id')) {
            $timeslot = Timeslot::findOrFail($request->input('timeslot_id'));
            if($timeslot = $timeslot->delete()){
                return response()->json(['STATUS'=>'OK','ERR'=>'','message'=>trans('alerts.backend.timeslots.deleted')]);
        }else{
                return response()->json(['STATUS'=>'ERR','ERR'=>'DONSTSAVE']);
            }
        } else {
            abort(500);
        }
    }

    public function ScheduleCallsSave(Request $request)
    {
        if (request()->input('start')&&request()->input('stop')&&request()->input('type')){
            if (request()->input('id')) {
                $timeslot = Timeslot::findOrFail(request()->input('id'));
                $messOk=trans('alerts.backend.timeslots.updated');
            } else {
                $timeslot = new Timeslot();
                $messOk=trans('alerts.backend.timeslots.created');
            }
            $data = $request->all();
            unset($data['id']);
            $data['school_id']=$request->session()->get('schoold_id',$request->user()->school_id);
            $timeslot->fill($data);
            $timeslot->save();
            return response()->json(['STATUS'=>'OK','ERR'=>'','message'=>$messOk]);
        } else {
            return response()->json(['STATUS'=>'ERR','ERR'=>'DONTSAVE']);
        }

    }
}
