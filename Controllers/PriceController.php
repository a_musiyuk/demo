<?php

namespace App\Http\Controllers\Backend;

use App\Models\Category;
use App\Models\PriceProduct;
use App\Models\Product;
use App\Models\ProductPrice;
use App\Models\TypePrice;
use Arcanedev\LogViewer\Entities\Log;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class PriceController extends Controller
{
    public function index()
    {
        return view('backend.prices');
    }


    public function getPriceList(Request $request)
    {

        $sqlRAW='products.name as productname, products.sku as sku,  categories.name as categoryname , currencies.name as cur_name';
        foreach (TypePrice::actual() as $typePrice){
            $sqlRAW.=', MAX(if(product_prices.price_type_id='.$typePrice->id.',product_prices.price,0)) as typeprice_'.$typePrice->id.
                     ', MAX(if(product_prices.price_type_id='.$typePrice->id.',product_prices.rate,0)) as typeprice_'.$typePrice->id.'_rate';
        };
        $priceitems = DB::table('products')->select(DB::raw($sqlRAW))
            ->leftJoin('categories', 'products.category_id', '=', 'categories.id')
            ->Join('product_prices', 'products.id', '=', 'product_prices.product_id')
            ->Join('currencies', 'product_prices.currency_id', '=', 'currencies.id')
            ->groupBy('products.name','products.sku','categories.name','cur_name')
            ->where('products.company_id','=',session()->get('company_id'));


        $dtResponse = DataTables::of($priceitems)
            ->addColumn('rest', function($priceitem) {
              return 'Есть';
            })

            ->addColumn('actions', function($priceitem) {
                return  '';
            })
            ->rawColumns(['actions']);
        foreach (TypePrice::actual() as $typePrice){
            $dtResponse->editColumn('typeprice_'.$typePrice->id, function($priceitem) use ($typePrice) {
                if  ($priceitem->{'typeprice_'.$typePrice->id}==0) {
                    return '';
                } else {
                    $smUAH = number_format($priceitem->{'typeprice_'.$typePrice->id}*$priceitem->{'typeprice_'.$typePrice->id.'_rate'},2);
                    return  ''.$smUAH.'UAH / '.number_format($priceitem->{'typeprice_'.$typePrice->id},2).$priceitem->cur_name;

                }
            });
        }

        return $dtResponse
            ->filter(function ($query) use ($request) {
                \Illuminate\Support\Facades\Log::info('analyze filters');
                if ($request->get('lookfor')) {
                    \Illuminate\Support\Facades\Log::info('apply lookfor <<'.$request->get('lookfor'));
                    $query->whereRAW('categories.name like "%'.$request->get('lookfor').'%" OR products.name like "%'.$request->get('lookfor').'%"');
                }

                if ($request->has('category_id')&&$request->category_id ) {
                    if ($request->category_id<>'1') {
                        $arrIds=[];
                        $childs = Category::find($request->get('category_id'))->children()->get();
                        $arrIds[]=$request->category_id;
                        foreach ($childs as $child) {
                            $arrIds[]=$child->id;
                        }
                        \Illuminate\Support\Facades\Log::info('category filter '.print_r($arrIds).'  category_id = '.$request->get('categoryid'));
                        if (count($arrIds)==0&&$request->get('categoryid')) {
                            $arrIds[]=$request->get('categoryid');
                            $query->whereIn('category_id', $arrIds);
                        } elseif (count($arrIds)) {
                            $query->whereIn('category_id', $arrIds);
                        }


                    }
                }

            })
       -> make(true);

    }
}

