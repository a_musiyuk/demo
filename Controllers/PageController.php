<?php

namespace App\Http\Controllers;

use App\Page;
use App\Shop;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class PageController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function index()
    {
        $indexpage=1;
        return view('index')->with(compact('indexpage'));
    }



    public function cinema()
    {
        return redirect()->to('http://www.google.com');
    }

    public function map($floor=null, Request $request)
    {

        if ($floor==2)
            $floor_name = 'FLOOR2';
        elseif($floor==3)
            $floor_name = 'FLOOR3';
        else
            $floor_name = 'FLOOR1';

        $shops = Shop::published()->where('shop_level',$floor_name)->withTranslation()->get();
        return view('map',compact('shops','floor_name'));
    }

    public function agree()
    {
        return view('agreePD');
    }


    ///////

    private function getListPages($page_type,$page_name) {
        if($page_type)
            $pages = Page::published()->where('page_type',$page_type);
        else
            $pages = Page::where('id','>',0);

        $pages = $pages->withTranslation(LaravelLocalization::getCurrentLocale())->latest()->paginate(9);

        if ($page_type=='EVENT')
            $banner_type='EVENTS_SLIDE';
        elseif ($page_type=='PROMO')
            $banner_type='PROMO_SLIDE';
        elseif ($page_type=='NEWS')
            $banner_type='NEWS_SLIDE';
        elseif ($page_type=='')
            $banner_type='ALL_SLIDE';
        else
            $banner_type='OTHER_SLIDE';

        return view('page_list')->with(compact('pages','page_name','banner_type','page_type','back_route'));
    }

    private function getDetailPage($page_type,$slug_page){
        $page = Page::where('slug',$slug_page)->first();
        if ($page_type=='PROMO') {
            $page_name=trans('strings.promos');
            $back_route='promos';
            $banner_type='PROMO_SLIDE';
        }
        elseif ($page_type=='EVENT'){
            $page_name=trans('strings.events');
            $back_route='events';
            $banner_type='EVENTS_SLIDE';
        }
        elseif ($page_type=='OTHER'){

            $page_name='';
            $back_route='index';
            $banner_type='OTHER_SLIDE';

        }
        else{
            $page_name=trans('strings.news');
            $back_route='news';
            $banner_type='NEWS_SLIDE';
        }

        if ($page)
            return view('detail')->with(compact('page','page_type','page_name','back_route','banner_type'));
        else
            return abort(404);
    }

    public function allpages()
    {
        $page_type = trans('strings.all_pages');
        return $this->getListPages('',$page_type);
    }

    public function promos()
    {
        $page_type = trans('strings.promos');
        return $this->getListPages('PROMO',$page_type);
    }

    public function events()
    {
        $page_type = trans('strings.events');
        return $this->getListPages('EVENT',$page_type);

    }

    public function news()
    {
        $page_type = trans('strings.news');
        return $this->getListPages('NEWS',$page_type);

    }


    public function promo($tag_slug, Request $request)
    {

        return $this->getDetailPage('PROMO',$tag_slug);
    }

    public function event($tag_slug, Request $request)
    {
        return $this->getDetailPage('EVENT',$tag_slug);
    }

    public function newsone($tag_slug, Request $request)
    {
        return $this->getDetailPage('NEWS',$tag_slug);
    }

    public function otherpage($tag_slug, Request $request)
    {
        return $this->getDetailPage('OTHER',$tag_slug);
    }
}
