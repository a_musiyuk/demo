@extends('backend.layouts.app')

@section ('title', trans('strings.backend.products.title'))

@section('after-styles')
    {{ Html::style("css/backend/plugin/jstree/style.css") }}
@stop

@section('page-header')
    <h1>
        {{ app_name() }}
        <small>{{ trans('strings.backend.products.title') }}</small>
    </h1>
@endsection


@section('content')
    <h4>ТОВАРЫ</h4>

    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{trans('strings.backend.products.title_product') }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            <form method="POST" id="search-form" class="form-inline" role="form">
                <label for="lookfor">{{trans('strings.backend.general.filter')}}</label>
                <div class="input-group margin">
                    <!-- /btn-group -->
                    <input type="text"  class="form-control" name="lookfor">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-info">{{trans('strings.backend.general.filter')}}</button>
                    </div>

                </div>


            </form>

            <div class="col-md-4  pre-scrollable">
                <input type="hidden" id="selectcategory" name="selectcategory">
                <div id="container">

                </div>
            </div>
            <div class="col-md-8">
             <div class="table-responsive">
                <table id="products-table" class="table table-condensed table-hover">
                    <thead>
                    <tr>
                        <th>{{ trans('labels.cabinet.product.product.table.name') }}</th>
                        <th>{{ trans('labels.cabinet.product.product.table.sku') }}</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
            </div>
        </div><!-- /.box-body -->
    </div><!--box box-success-->


    <div class="modal fade" id="modal-inputcount" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">{{trans('labels.cabinet.product.add_to_cart') }}</h4>
                </div>
                <div class="modal-body" id="inputcount">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">{{trans('buttons.general.close')}}</button>
                    <button type="button" onclick="addToCart()" class="btn btn-primary">{{trans('buttons.general.addtocart')}}</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

@stop


@section('after-scripts')
    {{ Html::script("js/backend/plugin/datatables/jquery.dataTables.min.js") }}
    {{ Html::script("js/backend/plugin/datatables/dataTables.bootstrap.min.js") }}
    {{ Html::script("js/backend/plugin/jstree/jstree.js") }}

    <script>

        function showDialog(dialogType,product_id) {

            if (dialogType==1){
                console.log(dialogType);
                $.ajax({

                    url : '{{route('admin.showdialog_addtocart')}}?product_id='+product_id,
                    type : 'GET',
                    processData: false,
                    contentType: false,
                    success : function(data) {
                        console.log('lets show');
                        $("#inputcount").html(data);
                        $('#modal-inputcount').modal('show');

                    },
                    error: function(e) {
                        console.log(e);
                    }

                });


            }

        }



        function  addToCart() {

            var formData = new FormData();
            formData.append('product_id', $('#addtocart_product_id').val());
            formData.append('qty', $('#addtocart_amount').val());
            formData.append('price', $('#addtocart_price').val());
            formData.append('total', $('#addtocart_total').val());
            $.ajaxSetup({
                headers:{'X-CSRF-Token':$('meta[name=csrf-token]').attr("content")}
            });

            $.ajax({
                url : '{{route('admin.addToCart')}}',
                type : 'POST',
                data : formData,
                processData: false,
                contentType: false,
                success : function(data) {
                    var result = JSON.parse(data);
                    $('#count_in_cart').html(result.count_in_cart);
                    $('#modal-inputcount').modal('hide');
                },
                error: function(e) {
                    console.log(e);
                }

            });



        }


        $(function() {

            $('#container')
                    .on("changed.jstree", function (e, data) {
                        if(data.selected.length) {
                            $('#selectcategory').val(data.instance.get_node(data.selected[0]).id);
                            oTable.draw();
                        }
                    })
                    .jstree({
                'core' : {
                    'data' : {
                        "url" : "{{ route("admin.getCategoryTree") }}",
                    "dataType" : "json" // needed only if you do not supply JSON headers
                }
                }
            });

            var oTable = $('#products-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("admin.getProductList") }}',
                    type: 'get',
                    data: function (d) {
                       d.lookfor = $('input[name=lookfor]').val();
                       d.categoryid = $('#selectcategory').val();
                    }
                },
                columns: [
                    {data: 'name', name: 'name'},
                    {data: 'sku', name: 'sku'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500
            });
            $('#search-form').on('submit', function(e) {
                oTable.draw();
                e.preventDefault();
            });

        });
    </script>
@stop