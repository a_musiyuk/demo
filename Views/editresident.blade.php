@extends('backend.layouts.app')

@section('page-header')
    <h1>
        {{ app_name() }}
        {{$tbl_resident->fullname_resident}}
        <small>{{ trans('strings.oselya.editaccount') }} </small>
    </h1>
@endsection


@section('content')
    <style>
        tr.shown td.details-control {
            background: url({{env('APP_URL', '..')}}/img/details_close.png) no-repeat left center;
        }

        td.details-control {
            background: url({{env('APP_URL', '..')}}/img/details_open.png) no-repeat left center;
            cursor: pointer;
            width: 18px;
    </style>

    <form id="residentForm" method="post" class="form-horizontal" action="{{route('saveresident',[$id_house,$id_resident])}}">
        <div class="form-group"  >
            {{ csrf_field() }}
            <input type="hidden" name="id" id="id"  value="{{$id_resident}}">
            <input type="hidden" id="usertelefon" name="usertelefon" value="{{$tbl_resident->telefon_contact}}">
            <input type="hidden" id="all_list" name="all_list" value="{{$all_list}}">
            <input type="hidden" name="make_save" id="make_save"  value="{{$tbl_resident->id ? 0 : 1 }}">
            <input type="hidden" name="residentactivated" id="residentactivated" value="{{$tbl_resident->user_id==0 ? 0 :1 }}">

        </div>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">

                <li class="pull-left active"><a href="#main" data-toggle="tab" aria-expanded="false">Общие</a></li>
                @if (!$id_resident=="0")
                <li class="pull-left"><a href="#persons" data-toggle="tab" aria-expanded="false">Прописанные</a></li>

                <li class="pull-left"><a href="#counter" data-toggle="tab" aria-expanded="false">Счетчики</a></li>
                @endif

                <li class="pull-left"><a href="#balance" data-toggle="tab" aria-expanded="true">Баланс</a></li>
                @if (!$id_resident=="0")
                <li class="pull-left"><a href="#sybsidy" data-toggle="tab" aria-expanded="true">Субсидии</a></li>
                <li class="pull-left"><a href="#privilige" data-toggle="tab" aria-expanded="true">Льготы</a></li>
                @else
                    <li class="pull-right"> <button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i>Сохранить</button></li>
                @endif



            </ul>
            <div class="tab-content">

                <div class="tab-pane active" id="main">
                    <div class="box box-solid">
                        <div class="box-body">
                            <div class="margin">
                                <div class="btn-group">
                                @if ($tbl_resident->telefon_contact)
                                    @if($tbl_resident->user_id==0)
                                        <button type="button" onclick="activateUser()" id="btnActivate" class="btn btn-success"><i class="glyphicon glyphicon-user"></i>Разрешить доступ</button>
                                    @else
                                        <button type="button" onclick="activateUser()"  id="btnActivate" class="btn btn-danger"><i class="glyphicon glyphicon-user"></i>Запретить доступ</button>
                                    @endif
                                @endif
                                </div>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                                        <i class="glyphicon glyphicon-print"></i>&nbsp;&nbsp;Печать
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="javascript:makePrint('bill_one')">Квитанция</a></li>
                                        <li><a href="javascript:makePrint('spravka')">Справка</a></li>
                                    </ul>
                                </div>

                            </div>

                            <div class="form-group">

                                <label class ="control-label col-md-2 col-xs-6" for="house_id">Дом:</label>
                                <div class="col-md-2 col-xs-6">
                                    <select name="house_id" id="house_id">
                                        @foreach(\App\Models\House::all() as $house)
                                            <option value="{{$house->id}}" @if($house->id==$tbl_resident->house_id) selected @endif>{{$house->name_house}}</otion>
                                        @endforeach
                                    </select>
                                </div>

                                <label class ="control-label col-md-2 col-xs-2" for="numapp">№ квартиры:</label>
                                <div class="col-md-2 col-xs-6">
                                    <input type="text" data-history="1" class="form-control " id="numapp"  name="number_appart" value="{{$tbl_resident->number_appart}}" data-validation="required"  data-validation-error-msg="Укажите номер квартиры">
                                </div>
                                <label class ="control-label col-md-2  col-xs-4" for="acc">№ Л/С:</label>
                                <div class=" col-md-2 col-xs-6">
                                    <input type="text" data-history="1" class="form-control " id="acc"  name="acc" value="{{$tbl_resident->acc}}" data-validation="required"  data-validation-error-msg="Укажите номер ЛС">
                                </div>
                            </div>


                            <div class="form-group">
                                <label   class ="control-label col-md-2 col-xs-4" for="comment">Примечание:</label>
                                <div class="col-md-10 col-xs-6">
                                    <input type="text" data-history="1" class="form-control" id="comment" name="comment" value="{{$tbl_resident->comment}}">
                                </div>
                            </div>

                            @if ($tbl_resident->id)

                            <table id="treetable" class="table">
                                <colgroup>
                                    <col width="35%"></col>
                                    <col width="55%"></col>
                                    <col width="5%"></col>
                                    <col width="5%"></col>
                                </colgroup>
                                <thead>
                                <tr>
                                    <th>Параметр</th>
                                    <th>Значение</th>
                                    <th>История</th>
                                    <th>Изменить</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            @else
                            <div class="form-group">
                                <label class ="control-label col-md-2 col-xs-4" for="lname_resident">Фамилия:</label>
                                <div class=" col-md-2 col-xs-6">
                                    <input type="text" data-history="1" class="form-control" id="lname_resident" name="lname_resident"  value="{{$tbl_resident->lname_resident}}"   data-validation="required"  data-validation-error-msg="Укажите фамилию">
                                </div>
                                <label class ="control-label col-md-2 col-xs-4 " for="name_resident">Имя:</label>
                                <div class="col-md-2 col-xs-6">
                                    <input type="text" data-history="1" class="form-control" id="name_resident"  name="name_resident" value="{{$tbl_resident->name_resident}}"  data-validation="required"  data-validation-error-msg="Укажите имя">
                                </div>

                                <label   class ="control-label col-md-2 col-xs-4" for="mname_resident">Отчество:</label>
                                <div class="col-md-2 col-xs-6">
                                    <input type="text" data-history="1" class="form-control" id="mname_resident" name="mname_resident" value="{{$tbl_resident->mname_resident}}">
                                </div>

                            </div>

                            <div class="form-group">
                                <label class ="control-label col-md-2 "  for="telefon_home">Домашний телефон:</label>
                                <div class="col-md-2">
                                    <input type="text" data-history="1" class="form-control" id="telefon_home" name="telefon_home" value="{{$tbl_resident->telefon_home}}" >
                                </div>
                                <label class ="control-label col-md-2"  for="telefon_contact">Контактный телефон:</label>
                                <div class="col-md-2">
                                    <input type="text" data-history="1" class="form-control" id="telefon_contact" name="telefon_contact" value="{{$tbl_resident->telefon_contact}}" >
                                </div>

                                <label class ="control-label col-md-2 "  for="peoples_count">E-mail:</label>
                                <div class="col-md-2">
                                    <input type="text" data-history="1" class="form-control" id="email" name="email" value="{{$tbl_resident->email}}"  >
                                </div>

                            </div>

                            <div class="form-group">
                                <label class ="control-label col-md-2 "  for="totalarea_apart">Общая площадь:</label>
                                <div class="col-md-2">
                                    <input type="number"  data-history="1"  step="0.01"  class="form-control" id="totalarea_apart" name="totalarea_apart" value="{{$tbl_resident->totalarea_apart}}" >
                                </div>

                                <label class ="control-label col-md-2"  for="heat_apart">Отапливаемая площадь:</label>
                                <div class="col-md-2">
                                    <input type="number" data-history="1" step="0.01" class="form-control" id="heat_apart" name="heat_apart" value="{{$tbl_resident->heat_apart}}">
                                </div>

                                <label class ="control-label col-md-2 "  for="flat_count">Количество комнат:</label>
                                <div class="col-md-2">
                                    <input type="text" data-history="1" class="form-control" id="flat_count" name="flat_count" value="{{$tbl_resident->flat_count}}"  >
                                </div>

                            </div>

                            <div class="form-group">
                                <label class ="control-label col-md-2 "  for="lifesquare_appart">Жилая площадь:</label>
                                <div class="col-md-2">
                                    <input type="number" data-history="1"  step="0.01"  class="form-control" id="lifesquare_appart" name="lifesquare_appart" value="{{$tbl_resident->lifesquare_appart}}" >
                                </div>

                                <label class ="control-label col-md-2"  for="addedsquare_appart">Дополнительная площадь:</label>
                                <div class="col-md-2">
                                    <input type="number" data-history="1" step="0.01" class="form-control" id="addedsquare_appart" name="addedsquare_appart" value="{{$tbl_resident->addedsquare_appart}}">
                                </div>

                                <label class ="control-label col-md-2 "  for="peoples_count">Количество прописанных:</label>
                                <div class="col-md-2">
                                    <input type="text" data-history="1" class="form-control" id="peoples_count" name="peoples_count" value="{{$tbl_resident->peoples_count}}"  >
                                </div>

                            </div>


                            <div class="form-group">
                                <label class ="control-label col-xs-3 "  for="document_own">Документ о владении:</label>
                                <div class="col-xs-2">
                                    <input type="text" data-history="1" class="form-control" id="document_own" name="document_own" value="{{$tbl_resident->document_own}}"  >
                                </div>
                                <label class ="control-label col-xs-3 "  for="document_own_number">Номер документа:</label>
                                <div class="col-xs-2">
                                    <input type="text" data-history="1" class="form-control" id="document_own_number" name="document_own_number" value="{{$tbl_resident->document_own_number}}"  >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class ="control-label col-xs-3 "  for="document_own_date">Дата документа:</label>
                                <div class="col-xs-2">
                                    <input type="date" data-history="1" class="form-control" id="document_own_date" name="document_own_date" value="{{$tbl_resident->document_own_date ? $tbl_resident->document_own_date->format('Y-m-d') : ''}}"  >
                                </div>
                                <label class ="control-label col-xs-3 "  for="document_own_comment">Примечание:</label>
                                <div class="col-xs-2">
                                    <input type="text" data-history="1" class="form-control" id="document_own_comment" name="document_own_comment" value="{{$tbl_resident->document_own_comment}}"  >
                                </div>


                            </div>

                            <div class="form-group">
                                <label class ="control-label col-xs-3"  for="date_own">Дата владения:</label>
                                <div class="col-xs-2">
                                    <input type="date" data-history="1" class="form-control" id="date_own"  name="date_own" value="{{$tbl_resident->date_own ? $tbl_resident->date_own->format('Y-m-d') : ''}}"  >
                                </div>

                                <label class ="control-label col-xs-4"  for="percent_own">Процент владения:</label>
                                <div class="col-xs-1">
                                    <input type="number" data-history="1" class="form-control" id="percent_own"  name="percent_own" value="{{$tbl_resident->percent_own}}" >
                                </div>
                            </div>
                            @endif
                         </div>
                    </div>
                </div>
                @if (!$id_resident=="0")
                <div class="tab-pane" id="persons">
                    <div class="box box-solid">
                        <div class="box-body">
                            <div id="toolbar_person">
                                <div class="btn-group" role="group" aria-label="...">
                                    <div class="btn-group" role="group" aria-label="...">
                                        <button type="button"  class="btn btn-success" onclick="javascript: window.location='{{route('ps.editperson',["id_resident"=>$id_resident,"id_person"=>"0"])}}'"><i class="glyphicon glyphicon-plus"></i>
                                            Добавить                                     </button>
                                    </div>
                                </div>
                            </div>

                            <table data-toggle="table" id="apartpersons" data-toolbar="#toolbar_person" >
                                <thead>
                                <tr>
                                    <th data-field="typecounter">ФИО</th>
                                    <th data-field="serial">Отношение к владельцу</th>
                                    <th data-field="setup">Дата приписки</th>
                                    <th data-field="unsetup" >Дата выписки</th>
                                    <th data-field="action" >Действие</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($tbl_resident->apartmentpersons as $person)
                                    <tr data-expanded="true">
                                        <td>{{$person->fullname_person}}</td>
                                        <td>{{$person->relationperson->name}}</td>
                                        <td>{{$person->date_in=='' ? '' : \Carbon\Carbon::parse($person->date_in)->format("d-m-Y")}}</td>
                                        <td>{{$person->end_out=='' ? '' : \Carbon\Carbon::parse($person->date_out)->format("d-m-Y")}}</td>
                                        <td><a href="{{route('ps.editperson',['id_resident'=>$id_resident,'id_person'=>$person->id])}}" class="btn btn-xs btn-success"><i class="glyphicon glyphicon-edit"></i> Открыть</a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="counter">
                    <div class="box box-solid">
                        <div class="box-body">
                            <div id="toolbar">
                                <div class="btn-group" role="group" aria-label="...">
                                    <div class="btn-group" role="group" aria-label="...">
                                        <button type="button"  class="btn btn-success" onclick="javascript: window.location='{{route('editresidentcounter',["id_house"=>$id_house,"id_resident"=>$id_resident,"id_counter"=>"0"])}}'"><i class="glyphicon glyphicon-plus"></i>
                                            Добавить                                     </button>
                                    </div>
                                </div>
                            </div>

                            <table data-toggle="table" id="housecounter" data-toolbar="#toolbar" >
                                <thead>
                                <tr>
                                    <th data-field="typecounter">Вид счетчика</th>
                                    <th data-field="serial">Серийный номер</th>
                                    <th data-field="setup">Дата установки</th>
                                    <th data-field="unsetup" >Дата снятия</th>
                                    <th data-field="action" >Действие</th>

                                </tr>
                                </thead>

                                <tbody>
                                @foreach($tbl_resident->apartscounter as $counter)
                                    <tr data-expanded="true">
                                        <td>{{$counter->typecounter->name_typecounter}}</td>
                                        <td>{{$counter->serial_number}}</td>
                                        <td>{{\Carbon\Carbon::parse($counter->begin_date)->format("d-m-Y")}}</td>
                                        <td>{{$counter->end_date=='' ? '' : \Carbon\Carbon::parse($counter->end_date)->format("d-m-Y")}}</td>
                                        <td><a href="{{route('editresidentcounter',['id_house'=>$id_house,'id_resident'=>$id_resident,'id_conter'=>$counter->id])}}" class="btn btn-xs btn-success"><i class="glyphicon glyphicon-edit"></i> Открыть</a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @endif

                <div class="tab-pane" id="balance">
                    <div class="box box-solid">
                        <div class="box-body">
                            <table class="table table-hover" style="width:100%" id="table-balance">
                                <thead>
                                <tr>
                                    <th ></th>
                                    <th >Период</th>
                                    <th data-align="right">Начисления</th>
                                    <th data-align="right">Льгота</th>
                                    <th data-align="right">Оплата</th>
                                    <th data-align="right">Субсидия</th>
                                    <th data-align="right">Корректировка</th>
                                    <th data-align="right">Долг</th>

                                </tr>
                                </thead>

                            </table>
                        </div>
                    </div>
                </div>
                @if (!$id_resident=="0")
                <div class="tab-pane" id="sybsidy">
                    <div class="box box-solid">
                        <div class="box-body">
                            <table data-toggle="table" id="tbl_sybsidy" data-show-footer="true">
                                <thead>
                                <tr>
                                    <th >Номер субсидии</th>
                                    <th >Дата начала</th>
                                    <th>Дата окончания</th>
                                    <th>Назначение</th>
                                    <th>Оплата</th>
                                    <th data-align="right">Субсидия</th>
                                    <th data-align="right">Обязательный платеж</th>
                                    <th>Примечание</th>
                                    <th>Действия</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if ($tbl_resident->subsidy)
                                    @foreach($tbl_resident->subsidy as $curSubsidy)
                                        <tr data-expanded="true">
                                            <td>{{$curSubsidy->id_external}}</td>
                                            <td>{{$curSubsidy->date_begin=='' ? '' : \Carbon\Carbon::parse($curSubsidy->date_begin)->format("d-m-Y")}}</td>
                                            <td>{{$curSubsidy->date_end=='' ? '' : \Carbon\Carbon::parse($curSubsidy->date_end)->format("d-m-Y")}}</td>
                                            <td>{{$curSubsidy->grouprate->shortname_grouprate}}</td>
                                            <td>{{$curSubsidy->sum_basesum}}</td>
                                            <td>{{$curSubsidy->sum_subsidy}}</td>
                                            <td>{{$curSubsidy->sum_required}}</td>
                                            <td>{{$curSubsidy->comments}}</td>
                                            <td><a href="{{route('subs.edit',['id_resident'=>$id_resident,'id_subsidy'=>$curSubsidy->id])}}" class="btn btn-xs btn-success"><i class="glyphicon glyphicon-edit"></i> Открыть</a></td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="privilige">
                    <div class="box box-solid">
                        <div class="box-body">
                            <div id="toolbar_priv">
                                <div class="btn-group" role="group" aria-label="...">
                                    <div class="btn-group" role="group" aria-label="...">
                                        <button type="button"  class="btn btn-success" onclick="javascript: window.location='{{route('priv.edit',["resident_id"=>$tbl_resident->id, "respriv_id"=>"0"])}}'"><i class="glyphicon glyphicon-plus"></i>
                                            Добавить                                     </button>
                                    </div>
                                </div>
                            </div>

                            <div class="box-body">
                                <table data-toggle="table" id="tbl_privilige" data-toolbar="#toolbar_priv" >
                                    <thead>
                                    <tr>
                                        <th >Льгота</th>
                                        <th >Дата начала</th>
                                        <th >Дата окончания</th>
                                        <th>Количество</th>
                                        <th >Примечание</th>
                                        <th >Действия</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if ($tbl_resident->residentpriviliges)
                                        @foreach($tbl_resident->residentpriviliges as $curPriv)
                                            <tr data-expanded="true">
                                                <td>{{$curPriv->privilige->name_privilige}}</td>
                                                <td>{{$curPriv->date_begin=='' ? '' : \Carbon\Carbon::parse($curPriv->date_begin)->format("d-m-Y")}}</td>
                                                <td>{{$curPriv->date_end=='' ? '' : \Carbon\Carbon::parse($curPriv->date_end)->format("d-m-Y")}}</td>
                                                <td>{{$curPriv->people_count}}</td>
                                                <td>{{$curPriv->comments}}</td>
                                                <td><a href="{{route('priv.edit',["resident_id"=>$tbl_resident->id,'respriv_id'=>$curPriv->id])}}" class="btn btn-xs btn-success"><i class="glyphicon glyphicon-edit"></i> Открыть</a></td>
                                            </tr>
                                        @endforeach
                                    @endif

                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>

    </form>


<!-- Modal -->
<div class="modal fade" id="resident_history" role="document">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">История изменений</h4>
            </div>
            <div class="modal-body">
                <table id="history_values_table" class=" table table-bordered table-hover" >
                    <thead>
                    <tr>
                        <th>Период изменения</th>
                        <th>Значение</th>
                        <th>Примечание</th>
                        <th>Редактор</th>
                        <th>Удалить</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="resident_editattrib" role="document">
        <div class="modal-dialog" >
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Новое значение</h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal" id="arrtibute_value_content">
                        <div class="form-group">
                            <label   class ="control-label col-md-5 col-xs-12" for="new_period">Дата для нового значения:</label>
                            <div class="col-md-5 col-xs-12">
                                <input type="date"  class="form-control" id="new_period" name="new_period" value="{{date('Y-m-d')}}">
                            </div>
                        </div>
                        <div id="resident_editattrib_content">


                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default"  onclick="saveAttribValue()">Записать</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>


    @include('includes.dilalogs.dialog_print')
@endsection

@section('after-scripts')

<script id="balancedetail-template" type="text/x-handlebars-template">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Детализация</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            <table class="table details-table" id="balancedetails-@{{period_transaction}}">
                <thead>
                <tr>
                    <th>Дата</th>
                    <th>Тариф</th>
                    <th>Начисления</th>
                    <th>Льгота</th>
                    <th>Оплата</th>
                    <th>Субсидия</th>
                    <th>Корректировка</th>
                    <th>Примечание</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</script>
{!! style('css/jquery-ui.css') !!}
{!! style('css/fancytree/ui.fancytree.css') !!}
{!! script('js/jquery-ui.js') !!}
{!! script('js/jquery.fancytree.js') !!}
{!! script('js/jquery.fancytree.table.js') !!}
{!! script('js/handlebars-v4.0.10.js') !!}

<!-- App scripts -->
<script>

    $('#house_id').select2({width: '100%'});

    @if (!$id_resident=="0")
    var template = Handlebars.compile($("#balancedetail-template").html());

    var table = $('#table-balance').DataTable({
        dom: '<"top"pli>rt<"bottom"><"clear">',
        processing: true,
        serverSide: true,
        "lengthMenu": [[50, 150, 200, -1], [50, 150, 200, "All"]],
        ajax: {
            url:'{{route('getBalansesPerMonthList',['resident_id'=>$tbl_resident->id])}}'
        },
        columns: [
            {
                "className":      'details-control',
                "orderable":      false,
                "searchable":      false,
                "data":           null,
                "defaultContent": ''
            },
            {data: 'period_transaction', name: 'period_transaction',orderable:true},
            {data: 'income', name: 'income'},
            {data: 'privilige', name: 'privilige'},
            {data: 'payment', name: 'payment'},
            {data: 'subsidy', name: 'subsidy'},
            {data: 'correct', name: 'correct'},
            {data: 'borg', name: 'borg'}
        ],
        order: [[0, 'asc']]
    });
    // Attach the fancytree widget to an existing <div id="tree"> element
    // and pass the tree options as an argument to the fancytree() function:
    var treeView = $("#treetable").fancytree({
        extensions: ["table"],
        /*                                                checkbox: true,*/
        table: {
            indentation: 20,      // indent 20px per node level
            /*
             nodeColumnIdx: 2,     // render the node title into the 2nd column
             checkboxColumnIdx: 0  // render the checkboxes into the 1st column
             */
        },
        source: {
            url: "{{route('getresidentattributes',['resident_id'=>($tbl_resident->id ? $tbl_resident->id : 0 ) ])}}"
        },

        renderColumns: function(event, data) {
            var node = data.node,
                    $tdList = $(node.tr).find(">td");
            if (node.children) {
                $tdList.eq(1).text('');
                $tdList.eq(2).html('');
                $tdList.eq(3).html('');

            } else {
                $tdList.eq(1).text(node.data.vls);
                $tdList.eq(2).html('<a href="javascript:showHistory('+node.data.id+')" class="btn btn-xs btn-success"><i class="glyphicon glyphicon-list-alt"></i></a>');
                $tdList.eq(3).html('<a href="javascript:editAttribute('+node.data.id+')" class="btn btn-xs btn-success"><i class="glyphicon glyphicon-edit"></i></a>');

            }
        }
    });
    /* Handle custom checkbox clicks */
    $("#treetable").delegate("input[name=like]", "click", function(e){
        var node = $.ui.fancytree.getNode(e),
                $input = $(e.target);
        e.stopPropagation();  // prevent fancytree activate for this row
    });

    // Add event listener for opening and closing details
    $('#table-balance tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var tableId = 'balancedetails-' + row.data().period_transaction;

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            row.child(template(row.data())).show();
            initTable(tableId, row.data());
            tr.addClass('shown');
            tr.next().find('td').addClass('no-padding');
        }
    });

    function initTable(tableId, data) {
        $('#' + tableId).DataTable({
            processing: true,
            serverSide: true,
            ajax: data.details_url,
            bPaginate: false,
            columns: [
                { data: 'period', name: 'period' },
                { data: 'rate_name', name: 'rate_name' },
                { data: 'income', name: 'income' },
                { data: 'privilige', name: 'privilige' },
                { data: 'payment', name: 'payment' },
                { data: 'subsidy', name: 'subsidy' } ,
                { data: 'correct', name: 'correct' } ,
                { data: 'comment', name: 'comment' } ,
            ]
        })
    }
    @endif

    function makePrint(typeDialog) {
        console.log("Begin edit");

        $.ajaxSetup({
            headers:{'X-CSRF-Token':$('input[name=_token]').val()}
        });

        $.ajax({
            url : '{{route('printdialog')}}?typedialog='+typeDialog+'&resident_id={{$tbl_resident->id}}',
            type : 'GET',
            processData: false,
            contentType: false,
            success : function(data) {

                $("#print_dialog_content").html(data);
                $("#print_dialog").modal();
            },
            error: function(e) {
                console.log(e);
            }

        });

    }

    function activateUser(activate) {

        if (!$('#account').val()) {
            alert("Не указан лицевой счет....");
            return;
        }


        var formData = new FormData();
        formData.append('idresident', $('#id').val());
        formData.append('idhouse', $('#house_id').val());
        formData.append('usertelefon', $('#usertelefon').val());
        formData.append('residentactivated', $('#residentactivated').val());

        $.ajaxSetup({
            headers:{'X-CSRF-Token':$('input[name=_token]').val()}
        });

        $.ajax({
            url : '{{route('activateresident')}}',
            type : 'POST',
            data : formData,
            processData: false,
            contentType: false,
            success : function(data) {
                var result = JSON.parse(data);
                if (result.err="OK") {
                    toastr.info(
                            "Пользователь успешно "+result.message

                    );

                    if ($('#residentactivated').val()=='1') {
                        $('#residentactivated').val(0);
                        $('#btnActivate').text('<i class="glyphicon glyphicon-user"></i>Разрешить доступ');
                        $('#btnActivate').addClass('btn-success');
                        $('#btnActivate').removeClass('btn-danger');
                    }else{
                        $('#residentactivated').val(1);
                        $('#btnActivate').html('<i class="glyphicon glyphicon-user"></i>Запретить доступ');
                        $('#btnActivate').addClass('btn-danger');
                        $('#btnActivate').removeClass('btn-success');

                    }

                } else {
                    toastr.danger(
                            result.message
                    );

                }

            },
            error: function(e) {
                toastr.danger(
                        "ОШИБКА Выполнения операции"

                );

            }

        });
    }

    function submitForm(event) {

        $('#tablecountersdata').val(JSON.stringify($('#tablecounters').bootstrapTable('getData')));
        $('#usertelefon').val($('#telefon_contact').val());


    };

    function showHistory(attrib_id) {
            $('#history_values_table').DataTable().destroy();
            $('#history_values_table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{route('gethistoryattribute',['resident_id'=>$tbl_resident->id])}}?attrib_id='+attrib_id,
                bPaginate: false,
                columns: [
                    { data: 'period', name: 'period' },
                    { data: 'value', name: 'value' },
                    { data: 'comment', name: 'comment' },
                    { data: 'user', name: 'user' },
                    { data: 'action', name: 'action' } ,
                ]
            })
        $('#resident_history').modal();
    }

    function editAttribute(attrib_id) {

        $.ajax({
            url : '{{route('getdialoghistoryattribute')}}?attribute_id='+attrib_id,
            type : 'GET',
            processData: false,
            contentType: false,
            success : function(data) {
                $("#resident_editattrib_content").html(data);
                $("#resident_editattrib").modal();
            },
            error: function(e) {
                console.log(e);
            }
        });
    }

    function saveAttribValue() {


        if (!$('#new_period').val()) {
            alert("Не указана дата для записи нового значения....");
            return;
        }


        if (!$('#new_value').val()) {
            alert("Не указано новое значение...");
            return;
        }


        var formData = new FormData();
        formData.append('resident_id', {{$tbl_resident->id}});
        formData.append('attribute_id', $('#new_attrib_id').val());
        formData.append('new_value', $('#new_value').val());
        formData.append('new_period', $('#new_period').val());
        formData.append('new_comment', $('#new_comment').val());
        $.ajaxSetup({
            headers:{'X-CSRF-Token':$('input[name=_token]').val()}
        });

        $.ajax({
            url : '{{route('sethistoryattribute')}}',
            type : 'POST',
            data : formData,
            processData: false,
            contentType: false,
            success : function(data) {
                var result = JSON.parse(data);
                if (result.status=="ERR") {
                    toastr.danger(result.errors);
                } else {
                    toastr.info("Реквизит успешно сохранен");

                }
                $("#treetable").fancytree();
                $("#resident_editattrib").modal('hide');

            },
            error: function(e) {
                console.log(e);
            }

        });

    }

    function addCounter(Name, idtypecounter) {
        var $table = $('#tablecounters');
        var _options='{!! \App\Models\Rate::getRateListofCounters() !!}}';
        $table.bootstrapTable('append', {typecounter:Name, serial:"<input type='text' id='serial' name='serial[]'  data-validation='required'  data-validation-error-msg='Укажите серийный номер'  >", rate: "<select class='col-xs-12' id='rate' name='rate[]'>"+
        _options+"</select>" ,setup:"<input type='date' id='setup' name='setup[]'>", unsetup:"<input type='date' id='unsetup' name='unsetup[]'>", id:"<input type='hide' id='id' name='id[]' value='0'>", idtypecounter:"<input type='hide' id='idtype' value='"+idtypecounter+"'name='id[]'>"});
        $table.bootstrapTable('scrollTo', 'bottom');
        $.validate();
};

</script>
@endsection


