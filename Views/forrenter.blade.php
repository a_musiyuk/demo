@extends('layouts.app')

@section('title',\App\AdditionalPage::getTitle())
@section('meta_description',\App\AdditionalPage::getDescription())
@section('meta_keywords',\App\AdditionalPage::getKeywords())

@section('content')
    @include('includes.banners',['banner_type'=>'ADDPAGE_SLIDE','gallary'=>''])

    <section class="default-section">
        <div class="container">
            <div class="breadcrumbs-panel wow fadeInUp" data-wow-delay=".35s" style="visibility: visible; animation-delay: 0.35s; animation-name: fadeInUp;">
                @include('includes.breadcrumbs',['crumbs'=>[['name'=>trans('strings.forrenter'),'route'=>'']]])
            </div>

            <div class="section-content wow fadeInUp" data-wow-delay=".35s" style="visibility: visible; animation-delay: 0.35s; animation-name: fadeInUp;">
                <h1>{{get_prop('pages.forrenter.caption')}}</h1>
                {!! \App\ContentsBlock::getBlock('for_renter_head') !!}
            </div>

            <form id="frmSendRequest" action="{{route('forrenter.store')}}"  method="POST" enctype="multipart/form-data"  class="default-form style2 wow fadeInUp" data-wow-delay=".35s" style="visibility: visible; animation-delay: 0.35s; animation-name: fadeInUp;">
                {{ csrf_field() }}
                <fieldset>
                    <div class="col-row">
                        <div class="col l-6 m-12">
                            <div class="form-row required">
                                <label>{{trans('strings.pages.forenter.company')}}</label>
                                <input type="text" class="text" required="" name="company">
                                <span class="error-msg">{{trans('strings.pages.common.incorrect_data')}}</span>
                            </div>
                            <div class="form-row">
                                <label>{{trans('strings.pages.forenter.square_type')}}</label>
                                <select name="type_area_id">
                                    @foreach(\App\AreaType::withTranslation()->active()->get() as $area_type)
                                        <option selected value="{{$area_type->id}}">{{$area_type->getTranslatedAttribute('name')}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-row required">
                                <label>{{trans('strings.pages.forenter.profile')}}</label>
                                <input type="text" class="text" value="" required="" name="profile">
                                <span class="error-msg">{{trans('strings.pages.common.incorrect_data')}}</span>
                            </div>
                            <div class="form-row required">
                                <label>{{trans('strings.pages.forenter.trademakrs')}}</label>
                                <input type="text" class="text" required="" name="trademarks" value="{{old('trademarks')}}">
                                <span class="error-msg">{{trans('strings.pages.common.incorrect_data')}}</span>
                            </div>
                            <div class="form-row required">
                                <label>{{trans('strings.pages.forenter.square_size')}}</label>
                                <div class="inline-inputs">
                                    {{trans('strings.pages.forenter.from')}}
                                    <div class="input-holder">
                                        <input type="text" class="text num" required="" name="square_from">
                                    </div>
                                    {{trans('strings.pages.forenter.to')}}
                                    <div class="input-holder">
                                        <input type="text" class="text num" required="" name="square_to">
                                    </div>
                                    м<sup>2</sup>
                                </div>
                            </div>
                        </div>
                        <div class="col l-6 m-12">
                            <div class="form-row required">
                                <label> {{trans('strings.pages.forenter.contact')}}</label>
                                <input type="text" class="text" required="" name="contact">
                                <span class="error-msg">{{trans('strings.pages.common.incorrect_data')}}</span>
                            </div>
                            <div class="form-row required">
                                <label> {{trans('strings.pages.forenter.email')}}</label>
                                <input type="email" class="text" value="" required="" name="email">
                                <span class="error-msg">{{trans('strings.pages.common.incorrect_data')}}</span>
                            </div>
                            <div class="form-row required">
                                <label> {{trans('strings.pages.forenter.phone')}}</label>
                                <input type="tel" name="phone" class="text" placeholder="+380 (__) ___ __ __" data-mask="+380 (99) 999 99 99" required="">
                                <span class="error-msg">{{trans('strings.pages.common.incorrect_data')}}</span>
                            </div>
                            <div class="form-row">
                                <label> {{trans('strings.pages.forenter.docs')}}</label>
                                <span class="input">
                                             {{trans('strings.pages.forenter.addfile')}}
                                            <input type="file" name="addedfiles">
                                            <span class="ico">
                                                <svg width="25" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 25 24" style="enable-background:new 0 0 25 24;" xml:space="preserve"><path d="M24.2,16.5c-0.4,0-0.8,0.3-0.8,0.8V19c0,1.9-1.6,3.5-3.5,3.5H5c-1.9,0-3.5-1.6-3.5-3.5v-1.8c0-0.4-0.3-0.8-0.8-0.8S0,16.8,0,17.2V19c0,2.8,2.3,5,5,5h15c2.8,0,5-2.2,5-5v-1.7C25,16.9,24.7,16.5,24.2,16.5z M12,18.9c0.1,0.1,0.3,0.2,0.5,0.2c0.2,0,0.4-0.1,0.5-0.2L18,14c0.3-0.3,0.3-0.8,0-1.1s-0.8-0.3-1.1,0l-3.6,3.6V0.8c0-0.4-0.3-0.8-0.8-0.8c-0.4,0-0.8,0.3-0.8,0.8v15.7l-3.6-3.6c-0.3-0.3-0.8-0.3-1.1,0S6.7,13.6,7,14L12,18.9z"></path></svg>
                                            </span>
                                        </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-bottom-holder">
                        <div class="form-row">
                            <div>
                                <label class="checkbox">
                                    <input required type="checkbox" >
                                    <i></i>
                                    {{trans('strings.pages.common.accpet_pdata1')}} <a href="{{route('agree')}}" target="_blank">{{trans('strings.pages.common.accpet_pdata2')}} </a>
                                </label>
                            </div>
                        </div>
                        <div class="form-bottom-panel">
                            {!! Captcha::display('forrenter',['data-callback'=>'enabledSend']) !!}
                            <span class="required-text"><i>*</i>{{trans('strings.pages.forenter.require_field')}}</span>
                        </div>
                    </div>
                    <div class="btn-panel">
                        <button id="btnSend"  class="btn btn-sub rounded" style="display: none;" >{{trans('strings.pages.forenter.sendrequest')}}</button>
                    </div>
                </fieldset>
            </form>
        </div>
    </section>
    <script>
        function enabledSend() {
            $('#btnSend').show();
        }

    </script>
    {!! Captcha::scriptWithCallback(['forrenter']) !!}
@endsection

@push('after-script')
@include('includes.toastr')
@endpush