<?php

namespace App\Console\Commands;

use App\AdditionalPage;
use App\Page;
use App\Shop;
use Illuminate\Console\Command;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\SitemapGenerator;
use Spatie\Sitemap\Tags\Url;

class generateSiteMap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate sitemap';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sitemap = Sitemap::create();

        AdditionalPage::all()->each(function (AdditionalPage $project) use ($sitemap) {
            $sitemap->add(Url::create(env('APP_URL')."/{$project->slug}"));
        });


        Shop::all()->each(function (Shop $shop) use ($sitemap) {
            $sitemap->add(Url::create(env('APP_URL')."/".get_sub_url($shop->shop_type)."/{$shop->slug}"));
        });

        Page::all()->each(function (Page $page) use ($sitemap) {
            $sitemap->add(Url::create(env('APP_URL')."/".get_sub_url($page->page_type)."/{$page->slug}"));
        });

        $sitemap->writeToFile(public_path('sitemap.xml'));
    }
}
